package ua.epam.spring.hometask.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ua.epam.spring.hometask.configuration.ApplicationConf;
import ua.epam.spring.hometask.configuration.AspectConf;
import ua.epam.spring.hometask.configuration.AuditoriumConf;
import ua.epam.spring.hometask.configuration.DiscountConf;
import ua.epam.spring.hometask.service.AuditoriumService;
import ua.epam.spring.hometask.service.BookingService;
import ua.epam.spring.hometask.service.EventService;
import ua.epam.spring.hometask.service.UserService;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.assertEquals;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConf.class, AspectConf.class, AuditoriumConf.class, DiscountConf.class})
public class IntegrationTest {

    @Autowired
    private AuditoriumService iAuditoriumService;
    @Autowired
    private BookingService iBookingService;
    @Autowired
    private EventService iEventService;
    @Autowired
    private UserService iUserService;
    private Set<Long> seats;

    @Before
    public void setUp() {


        NavigableSet<LocalDateTime> dateTimes = new TreeSet<>();
        dateTimes.add(LocalDateTime.of(2017, 10, 10, 10, 00, 00));
        dateTimes.add(LocalDateTime.of(2017, 10, 11, 11, 00, 00));

        NavigableMap<LocalDateTime, Auditorium> auditoriums = new TreeMap<>();
        auditoriums.put(LocalDateTime.of(2017, 10, 10, 10, 00, 00),
                iAuditoriumService.getByName("Blue auditorium"));
        auditoriums.put(LocalDateTime.of(2017, 10, 11, 11, 00, 00),
                iAuditoriumService.getByName("Green auditorium"));


        Event eventOne = new Event();
        eventOne.setName("The Fate Of The Furious");
        eventOne.setRating(EventRating.HIGH);
        eventOne.setAirDates(dateTimes);
        eventOne.setAuditoriums(auditoriums);
        eventOne.setBasePrice(new BigDecimal(100));
        iEventService.save(eventOne);

        seats = new HashSet<>();
        seats.add(3L);
        seats.add(4L);
        seats.add(10L);
    }

    @Test
    public void testFlow() {

        //Step 1
        //Create and save user to memory storage
        User userOne = new User("Misha", "Petrov", "m.petrov@gmail.com",
                LocalDateTime.of(2010, 10, 13, 10, 00, 00));
        iUserService.save(userOne);
        System.out.println(userOne.toString());

        //Check count of users in storage
        assertEquals("Storage has 1 user", iUserService.getAll().size(), 1);

        //Step 2
        //Getting and choose events
        List<Event> events = new ArrayList<>(iEventService.getAll());
        assertEquals("Storage has 1 event", events.size(), 1);

        System.out.println("List of events which user can choose!");
        for (Event event : events) {
            System.out.println("Name of event - " + event.getName() + " and rating - " + event.getRating());
            System.out.println("List of available auditoriums");
            for (Auditorium auditorium : event.getAuditoriums().values()) {
                System.out.println("Name - " + auditorium.getName() + "; Seats - " +
                        auditorium.getAllSeats().size() + "; Vip seats: " + auditorium.getVipSeats());
            }
        }

        Event event = iEventService.getByName("The Fate Of The Furious");

        //Step3
        //Create ticket
        Ticket ticketFirst = new Ticket(userOne, event,
                LocalDateTime.of(2017, 10, 7, 15, 00, 00), 10);
        ticketFirst.setId(Long.valueOf(1));
        Ticket ticketSecond = new Ticket(userOne, event,
                LocalDateTime.of(2017, 10, 8, 10, 00, 00), 7);
        ticketSecond.setId(Long.valueOf(2));
        Ticket ticketThird = new Ticket(userOne, event,
                LocalDateTime.of(2017, 10, 8, 10, 00, 00), 7);
        ticketThird.setId(Long.valueOf(3));
        Set<Ticket> tickets = new HashSet<>();
        tickets.add(ticketFirst);
        tickets.add(ticketSecond);
        tickets.add(ticketThird);


        //Step4
        //Book ticket
        iBookingService.bookTickets(tickets);
        //Get purchased tickets for event
        Set<Ticket> purchasedTicketsForEvent = iBookingService.getPurchasedTicketsForEvent(event,
                LocalDateTime.of(2017, 10, 8, 10, 00, 00));
        for (Ticket ticket : purchasedTicketsForEvent) {
            System.out.println("---------------------------");
            System.out.println("User name - " + ticket.getUser().getFirstName() + " " + ticket.getUser().getLastName());
            System.out.println("Date of ticket - " + ticket.getDateTime());
            System.out.println("Event - " + ticket.getEvent().getName());
            System.out.println("Seat - " + ticket.getSeat());
            System.out.println("---------------------------");
        }

        BigDecimal ticketsPrice = iBookingService.getTicketsPrice(event,
                LocalDateTime.of(2017, 10, 10, 10, 00, 00), userOne, seats);
        System.out.println("Tickets price - " + ticketsPrice);
    }
}
