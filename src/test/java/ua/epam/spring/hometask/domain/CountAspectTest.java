package ua.epam.spring.hometask.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ua.epam.spring.hometask.aspects.CountAspect;
import ua.epam.spring.hometask.configuration.ApplicationConf;
import ua.epam.spring.hometask.configuration.AspectConf;
import ua.epam.spring.hometask.configuration.AuditoriumConf;
import ua.epam.spring.hometask.configuration.DiscountConf;
import ua.epam.spring.hometask.service.BookingService;
import ua.epam.spring.hometask.service.EventService;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConf.class, AspectConf.class, AuditoriumConf.class, DiscountConf.class})
public class CountAspectTest {

    @Autowired
    private EventService iEventService;
    @Autowired
    private BookingService iBookingService;
    @Autowired
    private CountAspect countAspect;
    @Autowired
    private Event eventBean;


    @Before
    public void setUp() {
        Event event1 = new Event();
        event1.setName("Event1");
        event1.setBasePrice(new BigDecimal(100));
        event1.setRating(EventRating.LOW);

        Event event2 = new Event();
        event2.setName("Event2");
        event2.setBasePrice(new BigDecimal(1000));
        event2.setRating(EventRating.LOW);

        Event event3 = new Event();
        event3.setName("Event3");
        event3.setBasePrice(new BigDecimal(150));
        event3.setRating(EventRating.LOW);

        Event event4 = new Event();
        event4.setName("Event4");
        event4.setBasePrice(new BigDecimal(100));
        event4.setRating(EventRating.LOW);

        iEventService.save(event1);
        iEventService.save(event2);
        iEventService.save(event3);
        iEventService.save(event4);
    }

    @Test
    public void testCountAccessByName() {
        Event event1 = iEventService.getByName("Event1");
        Event event2 = iEventService.getByName("Event1");
        Event event3 = iEventService.getByName("Event1");

        assertEquals("Count eventByName is expected 3: ", Integer.valueOf(3),
                countAspect.getAccessByNameCounter().get("Event1"));

        Event event4 = iEventService.getByName("Event2");
        Event event5 = iEventService.getByName("Event2");
        Event event6 = iEventService.getByName("Event2");
        Event event7 = iEventService.getByName("Event2");
        Event event8 = iEventService.getByName("Event2");

        assertEquals("Count eventByName is expected 5: ", Integer.valueOf(5),
                countAspect.getAccessByNameCounter().get("Event2"));

    }

    @Test
    public void testCountGetPriceByName() {
        eventBean.setName("event");
        eventBean.getBasePrice();

        assertEquals("Count getPriceByName is expected 1: ", Integer.valueOf(1),
                countAspect.getGetPriceByNameCounter().get("event"));
    }

    @Test
    public void testCountBookTicketsByName() {
        Set<Ticket> tickets = new HashSet<>();

        User user = new User("user", "user", "email",
                LocalDateTime.of(2017, 10, 10, 00, 00,00));
        eventBean.setName("event1");
        Ticket ticket = new Ticket(user, eventBean,
                LocalDateTime.of(2017, 10, 10, 00, 00, 00), 10);
        ticket.setId(1L);
        tickets.add(ticket);

        iBookingService.bookTickets(tickets);
        assertEquals("Count book tickets by name of event is expected 1: ", Integer.valueOf(1),
                countAspect.getBookTicketsByNameCounter().get(eventBean.getName()));
    }
}
