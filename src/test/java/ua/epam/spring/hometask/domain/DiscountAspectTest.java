package ua.epam.spring.hometask.domain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ua.epam.spring.hometask.aspects.DiscountAspect;
import ua.epam.spring.hometask.configuration.ApplicationConf;
import ua.epam.spring.hometask.configuration.AspectConf;
import ua.epam.spring.hometask.configuration.AuditoriumConf;
import ua.epam.spring.hometask.configuration.DiscountConf;
import ua.epam.spring.hometask.service.DiscountService;
import ua.epam.spring.hometask.service.discounts.BirthdayStrategyImpl;
import ua.epam.spring.hometask.service.discounts.TicketsStrategyImpl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConf.class, AspectConf.class, AuditoriumConf.class, DiscountConf.class})
public class DiscountAspectTest {

    @Autowired
    private DiscountService iDiscountService;
    @Autowired
    private Event event;
    @Autowired
    private DiscountAspect discountAspect;

    @Test
    public void testDiscountCounter() {
        User user = new User("user", "user", "email",
                LocalDateTime.of(2000, 10, 10, 00, 00, 00));
        event.setName("event");
        event.setRating(EventRating.HIGH);
        event.setBasePrice(new BigDecimal(100));


        iDiscountService.getDiscount(user, event,
                LocalDateTime.of(2017, 10, 10, 00,00,00), 10);

        Map<String, Integer> mapBirthdatCounter = discountAspect.getDiscountCounterMap()
                .get(BirthdayStrategyImpl.class.getSimpleName());
        Map<String, Integer> mapTicketsCounter = discountAspect.getDiscountCounterMap()
                .get(TicketsStrategyImpl.class.getSimpleName());

        assertEquals("Counter for birthday discount is expected 1: ", Integer.valueOf(1),
                mapBirthdatCounter.get(user.getEmail()));
        assertEquals("Counter for tickets discount is expected 1: ", Integer.valueOf(1),
                mapTicketsCounter.get(user.getEmail()));
    }

}
