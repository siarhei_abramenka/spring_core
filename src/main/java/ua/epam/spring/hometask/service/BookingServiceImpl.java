package ua.epam.spring.hometask.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ua.epam.spring.hometask.domain.*;
import ua.epam.spring.hometask.repositories.IBookingDAO;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@Service
public class BookingServiceImpl implements BookingService{

    private IBookingDAO iBookingDAO;
    private DiscountService discountService;

    @Autowired
    public BookingServiceImpl(@Qualifier(value = "inMemoryBookingDAOImpl") IBookingDAO iBookingDAO,
                              DiscountService discountService) {
        this.iBookingDAO = iBookingDAO;
        this.discountService = discountService;
    }

    @Override
    public BigDecimal getTicketsPrice(@Nonnull Event event, @Nonnull LocalDateTime dateTime, @Nullable User user,
                                  @Nonnull Set<Long> seats) {
        List<Ticket> tickets = new ArrayList<>();

        for (Ticket ticket : iBookingDAO.getAll()) {
            if (seats.contains(ticket.getSeat())) {
                tickets.add(ticket);
            }
        }

        byte dicount = 0;
        for (int i = 0; i < tickets.size(); i++) {
            dicount = discountService.getDiscount(user, event, dateTime, i);
        }

        BigDecimal basePriceOfEvent = event.getBasePrice();

        Auditorium auditorium = event.getAuditoriums().get(dateTime);
        for (Long seat : seats) {
            if (auditorium.getVipSeats().contains(seat)) {
                basePriceOfEvent = (basePriceOfEvent.multiply(new BigDecimal(2)).multiply(new BigDecimal(dicount)))
                        .divide(new BigDecimal(100));
            }
        }

        if (event.getRating().equals(EventRating.HIGH)) {
            basePriceOfEvent = (basePriceOfEvent.multiply(new BigDecimal(1.2)).multiply(new BigDecimal(dicount)))
                    .divide(new BigDecimal(100));
        }

        return basePriceOfEvent.setScale(2, BigDecimal.ROUND_UP);
    }

    @Override
    public void bookTickets(@Nonnull Set<Ticket> tickets) {
        for (Ticket ticket : tickets) {
            if (ticket.getUser() != null) {
                ticket.getUser().getTickets().add(ticket);
                iBookingDAO.create(ticket);
            }

        }
    }

    @Nonnull
    @Override
    public Set<Ticket> getPurchasedTicketsForEvent(@Nonnull Event event, @Nonnull LocalDateTime dateTime) {
        List<Ticket> tickets = new ArrayList<>(iBookingDAO.getAll());
        Set<Ticket> purchasedTicketsForEvent = new TreeSet<>();

        for (Ticket ticket : tickets) {
            if (event.equals(ticket.getEvent()) && ticket.getDateTime().compareTo(dateTime) == 0) {
                purchasedTicketsForEvent.add(ticket);
            }
        }
        return purchasedTicketsForEvent;
    }
}
