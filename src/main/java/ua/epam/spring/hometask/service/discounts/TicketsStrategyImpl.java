package ua.epam.spring.hometask.service.discounts;

import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.DiscountService;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;

@Component("ticketsStrategyImpl")
public class TicketsStrategyImpl implements DiscountStrategy{

    @Override
    public byte calculateDiscount(@Nullable User user, @Nonnull Event event,
                                  @Nonnull LocalDateTime airDateTime, long numberOfTickets) {
        if (numberOfTickets % 10 == 0) {
            return 50;
        }
        return 0;
    }
}
