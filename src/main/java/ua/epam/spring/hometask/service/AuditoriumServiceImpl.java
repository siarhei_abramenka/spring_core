package ua.epam.spring.hometask.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ua.epam.spring.hometask.domain.Auditorium;
import ua.epam.spring.hometask.repositories.IAuditoriumDAO;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Set;


@Service
public class AuditoriumServiceImpl implements AuditoriumService {

    private IAuditoriumDAO iAuditoriumDAO;

    @Autowired
    public AuditoriumServiceImpl(@Qualifier(value = "inMemoryAuditoriumDAOImpl") IAuditoriumDAO iAuditoriumDAO) {
        this.iAuditoriumDAO = iAuditoriumDAO;
    }

    @Nonnull
    @Override
    public Set<Auditorium> getAll() {
        return (Set<Auditorium>) iAuditoriumDAO.getAll();
    }

    @Nullable
    @Override
    public Auditorium getByName(@Nonnull String name) {
        return iAuditoriumDAO.getByName(name);
    }
}
