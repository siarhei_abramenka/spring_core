package ua.epam.spring.hometask.service.discounts;

import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.DiscountService;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;

@Component("birthdayStrategyImpl")
public class BirthdayStrategyImpl implements DiscountStrategy{

    @Override
    public byte calculateDiscount(@Nullable User user, @Nonnull Event event,
                                  @Nonnull LocalDateTime airDateTime, long numberOfTickets) {
        LocalDateTime userBirthdayDate = user.getBirthdayDay();

        if (user.getBirthdayDay().getMonth().equals(airDateTime.getMonth())
                && userBirthdayDate.getDayOfMonth() - airDateTime.getDayOfMonth() <= 5) {
            return 5;
        }
        return 0;
    }
}
