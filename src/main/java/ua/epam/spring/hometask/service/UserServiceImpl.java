package ua.epam.spring.hometask.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.repositories.IUserDAO;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;

@Service
public class UserServiceImpl implements UserService{

    private IUserDAO iUserDAO;

    @Autowired
    public UserServiceImpl(@Qualifier(value = "inMemoryUserDAOImpl") IUserDAO iUserDAO) {
        this.iUserDAO = iUserDAO;
    }

    @Nullable
    @Override
    public User getUserByEmail(@Nonnull String email) {
        return iUserDAO.getByEmail(email);
    }

    @Override
    public User save(@Nonnull User object) {

        return iUserDAO.create(object);
    }

    @Override
    public void remove(@Nonnull User object) {
        iUserDAO.remove(object);
    }

    @Override
    public User getById(@Nonnull Long id) {
        return iUserDAO.getById(id);
    }

    @Nonnull
    @Override
    public Collection<User> getAll() {
        return iUserDAO.getAll();
    }
}
