package ua.epam.spring.hometask.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.repositories.IEventDAO;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class EventServiceImpl implements EventService{

    private IEventDAO iEventDAO;

    @Autowired
    public EventServiceImpl(@Qualifier(value = "inMemoryEventDAOImpl") IEventDAO iEventDAO) {
        this.iEventDAO = iEventDAO;
    }

    @Nullable
    @Override
    public Event getByName(@Nonnull String name) {
        return iEventDAO.getByName(name);
    }

    @Override
    public Event save(@Nonnull Event object) {
        return iEventDAO.create(object);
    }

    @Override
    public void remove(@Nonnull Event object) {
        iEventDAO.remove(object);
    }

    @Override
    public Event getById(@Nonnull Long id) {
        return iEventDAO.getById(id);
    }

    @Nonnull
    @Override
    public Collection<Event> getAll() {
        return iEventDAO.getAll();
    }

    @Nonnull
    @Override
    public Set<Event> getForDateRange(@Nonnull LocalDateTime from, @Nonnull LocalDateTime to) {
        List<Event> eventList = new ArrayList<>(iEventDAO.getAll());
        Set<Event> events = new TreeSet<>();

        for (Event event : eventList) {
            if (event.getAirDates().contains(from) || event.getAirDates().contains(to)) {
                events.add(event);
            }
        }
        return events;
    }

    @Nonnull
    @Override
    public Set<Event> getNextEvents(@Nonnull LocalDateTime to) {
        LocalDateTime from = LocalDateTime.now().withNano(0).withSecond(0);
        List<Event> eventList = new ArrayList<>(iEventDAO.getAll());
        Set<Event> events = new TreeSet<>();

        for (Event event : eventList) {
            if (event.getAirDates().contains(from) || event.getAirDates().contains(to)) {
                events.add(event);
            }
        }
        return events;
    }
}
