package ua.epam.spring.hometask.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.discounts.DiscountStrategy;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class DiscountServiceImpl implements DiscountService{

    private List<DiscountStrategy> discountStrategies;

    @Autowired
    public DiscountServiceImpl(@Value("#{allStrategies}") List<DiscountStrategy> discountStrategies) {
        this.discountStrategies = discountStrategies;
    }

    @Override
    public byte getDiscount(@Nullable User user, @Nonnull Event event,
                            @Nonnull LocalDateTime airDateTime, long numberOfTickets) {
        byte discount = 1;
        for (DiscountStrategy discountStrategy : discountStrategies) {
            byte maxDiscount = discountStrategy.calculateDiscount(user, event, airDateTime, numberOfTickets);
            if (maxDiscount > discount) {
                discount = maxDiscount;
            }
        }
        return discount;
    }
}
