package ua.epam.spring.hometask.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import ua.epam.spring.hometask.aspects.CountAspect;
import ua.epam.spring.hometask.aspects.DiscountAspect;

@Configuration
@EnableAspectJAutoProxy(proxyTargetClass=false)
@ComponentScan(value = "ua.epam.spring.hometask")
public class AspectConf {

    /*@Bean(name = "countAspect")
    public CountAspect getCountAspect() {
        return new CountAspect();
    }

    @Bean(name = "discountAspect")
    public DiscountAspect getDicountAspect() {
        return new DiscountAspect();
    }*/
}
