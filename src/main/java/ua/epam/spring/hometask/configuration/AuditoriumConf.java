package ua.epam.spring.hometask.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import ua.epam.spring.hometask.domain.Auditorium;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Configuration
@PropertySource({"classpath:auditoriums/auditoriumBlue.properties", "classpath:auditoriums/auditoriumGreen.properties",
"classpath:auditoriums/auditoriumRed.properties"})
@ComponentScan(value = "ua.epam.spring.hometask")
public class AuditoriumConf {

    @Value("${blue.name}")
    private String blueName;

    @Value("${blue.numberOfSeats}")
    private int blueNumberOfSeats;

    @Value("#{'${blue.vipSeats}'.split(',')}")
    private Set<Long> blueVipSeats;

    @Bean
    public Auditorium blueAuditorium() {
        Auditorium auditorium = new Auditorium();
        auditorium.setName(blueName);
        auditorium.setNumberOfSeats(blueNumberOfSeats);
        auditorium.setVipSeats(blueVipSeats);
        return auditorium;
    }

    @Value("${red.name}")
    private String redName;

    @Value("${red.numberOfSeats}")
    private int redNumberOfSeats;

    @Value("#{'${red.vipSeats}'.split(',')}")
    private Set<Long> redVipSeats;

    @Bean
    public Auditorium redAuditorium() {
        Auditorium auditorium = new Auditorium();
        auditorium.setName(redName);
        auditorium.setNumberOfSeats(redNumberOfSeats);
        auditorium.setVipSeats(redVipSeats);
        return auditorium;
    }

    @Value("${green.name}")
    private String greenName;

    @Value("${green.numberOfSeats}")
    private int greenNumberOfSeats;

    @Value("#{'${green.vipSeats}'.split(',')}")
    private Set<Long> greenVipSeats;

    @Bean
    public Auditorium greenAuditorium() {
        Auditorium auditorium = new Auditorium();
        auditorium.setName(greenName);
        auditorium.setNumberOfSeats(greenNumberOfSeats);
        auditorium.setVipSeats(greenVipSeats);
        return auditorium;
    }

    @Bean(name = "allAuditoriums")
    public List<Auditorium> getAuditoriums() {
        return Arrays.asList(blueAuditorium(), redAuditorium(), greenAuditorium());
    }
}
