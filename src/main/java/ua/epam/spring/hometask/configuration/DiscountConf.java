package ua.epam.spring.hometask.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ua.epam.spring.hometask.service.discounts.DiscountStrategy;

import java.util.Arrays;
import java.util.List;

@Configuration
@ComponentScan(value = "ua.epam.spring.hometask")
public class DiscountConf {

    @Autowired
    @Qualifier("birthdayStrategyImpl")
    private DiscountStrategy birhdayStrategy;

    @Autowired
    @Qualifier("ticketsStrategyImpl")
    private DiscountStrategy ticketsStrategy;

    @Bean(name = "allStrategies")
    public List<DiscountStrategy> getAllStrategies() {
        return Arrays.asList(birhdayStrategy, ticketsStrategy);
    }

}
