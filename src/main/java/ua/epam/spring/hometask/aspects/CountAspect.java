package ua.epam.spring.hometask.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.Ticket;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Aspect
@Component
public class CountAspect {

    private static final Map<String, Integer> accessByNameCounter = new HashMap<>();
    private static final Map<String, Integer> bookTicketsByNameCounter = new HashMap<>();
    private static final Map<String, Integer> getPriceByNameCounter = new HashMap<>();

    @Pointcut("execution(* ua.epam.spring.hometask.service.EventService.getByName(String)) && args(eventName))")
    private void accessedByName(String eventName) {
    }

    @Pointcut("(execution(* ua.epam.spring.hometask.domain.Event.getBasePrice(..)))")
    private void getPriceByName() {
    }

    @Pointcut("execution(* ua.epam.spring.hometask.service.BookingService.bookTickets(..)) " +
            "&& args(tickets)")
    private void bookTicketsByName(Set<Ticket> tickets) {
    }

    @AfterReturning(pointcut = "accessedByName(eventName)")
    public void countAccessByName(String eventName) {
        accessByNameCounter.put(eventName, accessByNameCounter.getOrDefault(eventName, 0) + 1);
    }

    @AfterReturning(pointcut = "bookTicketsByName(tickets)")
    public void countBookTicketsByName(Set<Ticket> tickets) {
        for (Ticket ticket : tickets) {
            if (ticket.getUser() != null) {
                bookTicketsByNameCounter.put(ticket.getEvent().getName(),
                        bookTicketsByNameCounter.getOrDefault(ticket.getEvent().getName(), 0) + 1);
            }
        }
    }

    @Before("getPriceByName()")
    public void countGetPriceByName(JoinPoint joinPoint) {
        Event event = (Event) joinPoint.getTarget();
        getPriceByNameCounter.put(event.getName(), getPriceByNameCounter.getOrDefault(event.getName(), 0) + 1);
    }

    public Map<String, Integer> getAccessByNameCounter() {
        return this.accessByNameCounter;
    }

    public Map<String, Integer> getBookTicketsByNameCounter() {
        return bookTicketsByNameCounter;
    }

    public Map<String, Integer> getGetPriceByNameCounter() {
        return getPriceByNameCounter;
    }

}
