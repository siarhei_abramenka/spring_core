package ua.epam.spring.hometask.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.discounts.BirthdayStrategyImpl;
import ua.epam.spring.hometask.service.discounts.TicketsStrategyImpl;

import java.util.HashMap;
import java.util.Map;

@Aspect
@Component
public class DiscountAspect {

    private static final Map<String, Map<String, Integer>> strategyMap = new HashMap<>();

    static {
        strategyMap.put(BirthdayStrategyImpl.class.getSimpleName(), new HashMap<>());
        strategyMap.put(TicketsStrategyImpl.class.getSimpleName(), new HashMap<>());
    }

    @Pointcut("(execution(* ua.epam.spring.hometask.service.discounts.DiscountStrategy.calculateDiscount(..)) " +
            "&& args(user, ..))")
    private void calculateDiscount(User user) {
    }

    @AfterReturning(pointcut = "calculateDiscount(user)", returning = "dis")
    public void countCalculateDiscount(JoinPoint joinPoint, User user, byte dis) {
        if (dis > 0) {
            final Class<?> discountStrategy = joinPoint.getTarget().getClass();
            if (discountStrategy.isAssignableFrom(BirthdayStrategyImpl.class)) {
                counter(BirthdayStrategyImpl.class.getSimpleName(), user);
            } else if (discountStrategy.isAssignableFrom(TicketsStrategyImpl.class)) {
                counter(TicketsStrategyImpl.class.getSimpleName(), user);
            } else {
                System.out.println("Unknown discount strategy: [" + discountStrategy.getName() + "]");
            }
        }
    }

    private void counter(String kindOfStrategy, User user) {
        Map<String, Integer> discountUserCounter = strategyMap.get(kindOfStrategy);
        discountUserCounter.put(user.getEmail(), discountUserCounter.getOrDefault(user.getEmail(), 0) + 1);
    }

    public Map<String, Map<String, Integer>> getDiscountCounterMap() {
        return strategyMap;
    }
}
