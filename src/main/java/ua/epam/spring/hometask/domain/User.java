package ua.epam.spring.hometask.domain;

import java.time.LocalDateTime;
import java.util.*;

public class User extends DomainObject {

    private String firstName;

    private String lastName;

    private String email;

    private LocalDateTime birthdayDay;

    private List<Ticket> tickets = new ArrayList<>();

    public User(String firstName, String lastName, String email, LocalDateTime birthdayDay) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.birthdayDay = birthdayDay;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDateTime getBirthdayDay() {
        return birthdayDay;
    }

    public void setBirthdayDay(LocalDateTime birthdayDay) {
        this.birthdayDay = birthdayDay;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, email);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        User other = (User) obj;
        if (email == null) {
            if (other.email != null) {
                return false;
            }
        } else if (!email.equals(other.email)) {
            return false;
        }
        if (firstName == null) {
            if (other.firstName != null) {
                return false;
            }
        } else if (!firstName.equals(other.firstName)) {
            return false;
        }
        if (lastName == null) {
            if (other.lastName != null) {
                return false;
            }
        } else if (!lastName.equals(other.lastName)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "User name: " + this.firstName + " " + this.lastName
                + ", User email: " + this.email + ", User birthday: " + this.birthdayDay;
    }
}
