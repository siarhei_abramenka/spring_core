package ua.epam.spring.hometask.repositories.InMemoryDAOImpl;

import java.util.*;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.repositories.IUserDAO;

import javax.annotation.Nonnull;

@Repository(value = "inMemoryUserDAOImpl")
public class InMemoryUserDAOImpl implements IUserDAO{

    private static final Map<String, User> USER_MAP = new TreeMap<>();

    @Override
    public User getByEmail(String email) {
        return USER_MAP.get(email);
    }

    @Override
    public User create(@Nonnull final User object) {
        USER_MAP.put(object.getEmail(), object);
        return object;
    }

    @Override
    public void remove(@Nonnull final User object) {
        USER_MAP.remove(object.getEmail());
    }

    @Override
    public User getById(@Nonnull Long id) {
        Collection<User> users = USER_MAP.values();
        User user = null;
        for (User us : users) {
            if (us.getId() == id) { user = us; }
        }
        return user;
    }

    @Nonnull
    @Override
    public Collection<User> getAll() {
        return USER_MAP.values();
    }
}
