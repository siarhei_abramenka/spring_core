package ua.epam.spring.hometask.repositories;

import ua.epam.spring.hometask.domain.Auditorium;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;

public interface IAuditoriumDAO{

    Auditorium getByName(String auditorium);


    @Nonnull
    Collection<Auditorium> getAll();
}
