package ua.epam.spring.hometask.repositories.InMemoryDAOImpl;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.repositories.IEventDAO;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

@Repository(value = "inMemoryEventDAOImpl")
public class InMemoryEventDAOImpl implements IEventDAO{

    private static final Map<String, Event> EVENT_MAP = new TreeMap<>();

    @Override
    public Event getByName(String eventName) {
        return EVENT_MAP.get(eventName);
    }

    @Override
    public Event create(@Nonnull Event object) {
        EVENT_MAP.put(object.getName(), object);
        return object;
    }

    @Override
    public void remove(@Nonnull Event object) {
        EVENT_MAP.remove(object.getName());
    }

    @Override
    public Event getById(@Nonnull Long id) {
        Event event = null;
        for (Event ev : EVENT_MAP.values()) {
            if (ev.getId() == id) {
                event = ev;
            }
        }
        return event;
    }

    @Nonnull
    @Override
    public Collection<Event> getAll() {
        return EVENT_MAP.values();
    }
}
