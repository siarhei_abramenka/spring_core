package ua.epam.spring.hometask.repositories;

import ua.epam.spring.hometask.domain.DomainObject;

import javax.annotation.Nonnull;
import java.util.Collection;

public interface IRepository<T extends DomainObject>{

    public T create(@Nonnull T object);

    public void remove(@Nonnull T object);

    public T getById(@Nonnull Long id);

    public @Nonnull
    Collection<T> getAll();
}
