package ua.epam.spring.hometask.repositories;

import ua.epam.spring.hometask.domain.Event;

import java.util.List;

public interface IEventDAO extends IRepository<Event>{

    Event getByName(String eventName);
}
