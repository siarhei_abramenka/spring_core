package ua.epam.spring.hometask.repositories.InMemoryDAOImpl;


import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.repositories.IBookingDAO;

import javax.annotation.Nonnull;
import java.util.*;

@Repository(value = "inMemoryBookingDAOImpl")
public class InMemoryBookingDAOImpl implements IBookingDAO {

    private static final Map<Long, Ticket> TICKET_MAP = new TreeMap<>();

    @Override
    public Ticket create(@Nonnull Ticket object) {
        TICKET_MAP.put(object.getId(), object);
        return object;
    }

    @Override
    public void remove(@Nonnull Ticket object) {
        TICKET_MAP.remove(object.getId());
    }

    @Override
    public Ticket getById(@Nonnull Long id) {
        return TICKET_MAP.get(id);
    }

    @Nonnull
    @Override
    public Collection<Ticket> getAll() {
        return TICKET_MAP.values();
    }
}
