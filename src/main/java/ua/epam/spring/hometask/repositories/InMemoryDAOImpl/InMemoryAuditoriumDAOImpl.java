package ua.epam.spring.hometask.repositories.InMemoryDAOImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import ua.epam.spring.hometask.domain.Auditorium;
import ua.epam.spring.hometask.repositories.IAuditoriumDAO;

import java.util.ArrayList;
import java.util.List;

@Repository(value = "inMemoryAuditoriumDAOImpl")
public class InMemoryAuditoriumDAOImpl implements IAuditoriumDAO{

    private List<Auditorium> auditoriums;

    @Autowired
    public InMemoryAuditoriumDAOImpl(@Value("#{allAuditoriums}") List<Auditorium> auditoriums) {
        this.auditoriums = auditoriums;
    }

    @Override
    public List<Auditorium> getAll() {
        return auditoriums;
    }

    @Override
    public Auditorium getByName(String auditoriumName) {

        Auditorium auditorium = null;
        for (Auditorium audit : auditoriums) {
            if (audit.getName().equals(auditoriumName)) {
                auditorium = audit;
            }
        }
        return auditorium;
    }
}
