package ua.epam.spring.hometask.repositories;

import ua.epam.spring.hometask.domain.User;

public interface IUserDAO extends IRepository<User> {

    User getByEmail(String email);
}
